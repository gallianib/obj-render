#pragma once

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void CharPress(GLFWwindow* window, unsigned int codepoint);
void MouseButton(GLFWwindow* window, int button, int action, int mods);
void MouseMotion(GLFWwindow* window, double x, double y);
void ReshapeWindow(GLFWwindow *, int width, int height);