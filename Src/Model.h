#pragma once
#include "Object.h"

struct _SimpleModel {
	std::map<std::string, _Object> objects;
	std::map<std::string, vec3> trans;
	std::map<std::string, vec3> scales;
	std::map<std::string, std::vector<vec4>> rotations;
	std::vector<vec3> lightPosition;
	std::vector<vec3> lightColor;

};

void ReadSimpleMdl(std::string filePath, _SimpleModel* mdl);
