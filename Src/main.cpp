// GLEW
#define GLEW_STATIC
/*
#ifdef __APPLE__
#include <OpenGL/glext.h>
#define glGenVertexArrays glGenVertexArraysAPPLE
#define glBindVertexArray glBindVertexArrayAPPLE
#define glDeleteVertexArrays glDeleteVertexArraysAPPLE
#else
#include <GL/glew.h>  // must come before the other GL includes
#endif
*/
#include <GL/glew.h>  // must come before the other GL includes

// GLFW
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Model.h"
#include "window.h"
#include "tga.h"
#include <math.h>
#include <cstring>
#define SIZEX 640
#define SIZEY 480
#define POSX 200
#define POSY 100
#define PI 3.14159265

using namespace std;

GLFWwindow *window;
GLuint program = 0;

bool wireframeMode = false;
bool leftMouseButtonDown = false;
double xpos, ypos;
mat4 modelView;
mat4 projection;

vec3 eye = vec3(-10, 60, -200);
vec3 at = vec3(10, 30, -100);
vec3 up = vec3(0, 1, 0);

int fov = 45;
GLfloat Settings1 = 1;
GLfloat Settings2 = 1;
GLfloat Settings3 = 1;
GLfloat Settings4 = 1;
GLfloat textureSettings = 1;
GLfloat lightMode = 1;
bool lightsMode = true;
GLfloat lightlevel = 800;


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE)
	{
		glfwDestroyWindow(window);
		glfwTerminate();
		exit(0);
	}
}
void CharPress(GLFWwindow* window, unsigned int codepoint) {
	if (char(codepoint) == 'q')
	{
		glfwDestroyWindow(window);
		glfwTerminate();
		exit(0);
	}
	if (char(codepoint) == 'w')
	{
		vec3 delta = 3*normalize(eye - at);
		eye = eye - delta;
		at = at - delta;
		GLuint gModelView = glGetUniformLocation(program, "ModelView");
		modelView = LookAt(eye, at, up);
		glUniformMatrix4fv(gModelView, 1, GL_TRUE, modelView);

	}
	if (char(codepoint) == 's')
	{
		vec3 delta = 3*normalize(eye - at);
		eye = eye + delta;
		at = at + delta;
		GLuint gModelView = glGetUniformLocation(program, "ModelView");
		modelView = LookAt(eye, at, up);
		glUniformMatrix4fv(gModelView, 1, GL_TRUE, modelView);

	}
	if (char(codepoint) == 'f')
	{
		if (wireframeMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			wireframeMode = false;
		}
		else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			wireframeMode = true;
		}
	}
	if (char(codepoint) == ',')
	{
		GLuint glightlevel = glGetUniformLocation(program, "lightlevel");
		lightlevel -= 100;
		glUniform1f(glightlevel, lightlevel);
	}
	if (char(codepoint) == '.')
	{
		GLuint glightlevel = glGetUniformLocation(program, "lightlevel");
		lightlevel += 100;
		glUniform1f(glightlevel, lightlevel);
	}
	if (char(codepoint) == 'r')
	{
		GLuint glightsMode = glGetUniformLocation(program, "lightMode");
		if (lightsMode)
		{
			lightsMode = false;
			lightMode = 0;
		}
		else {
			lightsMode = true;
			lightMode = 1;
		}
		glUniform1f(glightsMode, lightMode);
	}
	if (char(codepoint) == '1')
	{
		GLuint gSettings1 = glGetUniformLocation(program, "settings1");
		Settings1 = -Settings1;
		Settings2 = -1;
		Settings3 = -1;
		Settings4 = -1;
		glUniform1f(gSettings1, Settings1);
	}
	if (char(codepoint) == '2')
	{
		GLuint gSettings2 = glGetUniformLocation(program, "settings2");
		Settings2 = -Settings2;
		Settings1 = -1;
		Settings3 = -1;
		Settings4 = -1;
		glUniform1f(gSettings2, Settings2);
	}
	if (char(codepoint) == '3')
	{
		GLuint gSettings3 = glGetUniformLocation(program, "settings3");
		Settings3 = -Settings3;
		Settings2 = -1;
		Settings1 = -1;
		Settings4 = -1;
		glUniform1f(gSettings3, Settings3);
	}
	if (char(codepoint) == '4')
	{
		GLuint gSettings4 = glGetUniformLocation(program, "settings4");
		Settings4 = -Settings4;
		Settings2 = -1;
		Settings3 = -1;
		Settings1 = -1;
		glUniform1f(gSettings4, Settings4);
	}
	if (char(codepoint) == 't')
	{
		GLuint tSettings = glGetUniformLocation(program, "textureSettings");
		textureSettings = -textureSettings;
		glUniform1f(tSettings, textureSettings);
	}
}
void MouseButton(GLFWwindow* window, int button, int action, int mods) {

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		glfwGetCursorPos(window, &xpos, &ypos);
		leftMouseButtonDown = true;
	}else leftMouseButtonDown = false;
}
void MouseMotion(GLFWwindow* window, double x, double y) {
	if(leftMouseButtonDown){
		xpos = xpos - x;
		ypos = ypos - y;

		GLuint gModelView = glGetUniformLocation(program, "ModelView");

		vec3 delta = eye - at;
		vec3 n1 = CalcNormal(delta,up);
		at += n1 * xpos;
		vec3 n2 = CalcNormal(delta,n1);
		at += n2 * -ypos;
		modelView = LookAt(eye, at, up);

		glUniformMatrix4fv(gModelView, 1, GL_TRUE, modelView);

		xpos = x;
		ypos = y;
	}
}
void ReshapeWindow(GLFWwindow *, int width, int height) {
	//cout << "Width: " << width << " Height: " << height << endl;
	glViewport(0,0,width,height);
}

void err(int errcode, const char* desc)
{
	fprintf(stderr, "%d: %s\n", errcode, desc); return;
}

void InitWindow()
{
	window = glfwCreateWindow(SIZEX, SIZEY, "Render", NULL, NULL);
	if (!window) {
		glfwTerminate();
		//return -1;
	}

	glfwMakeContextCurrent(window);

	glfwSetWindowSizeCallback(window, ReshapeWindow);
	glfwSetCharCallback(window, CharPress);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, MouseMotion);
	glfwSetMouseButtonCallback(window, MouseButton);
}

void SetupView()
{
	GLuint gProjection = glGetUniformLocation(program, "Projection");
	GLuint gModelView = glGetUniformLocation(program, "ModelView");
	projection = Perspective(fov, (SIZEX / SIZEY), 1.0, 1000.0);

	modelView = LookAt(eye, at, up);
	glUniformMatrix4fv(gProjection, 1, GL_TRUE, projection);
	glUniformMatrix4fv(gModelView, 1, GL_TRUE, modelView);
}



mat4 GetModelMatrix(_SimpleModel mdl, string objName) {
	mat4 transmat = mat4(1.0f);
	mat4 scalemat = mat4(1.0f);
	mat4 rotationMat = mat4(1.0f);

	if (mdl.scales[objName].x != 0 || mdl.scales[objName].y != 0 || mdl.scales[objName].z != 0)
	{
		scalemat[0][0] = mdl.scales[objName].x;
		scalemat[1][1] = mdl.scales[objName].y;
		scalemat[2][2] = mdl.scales[objName].z;
	}
	
	
	for (size_t i = 0; i < mdl.rotations[objName].size(); i++)
	{
		double angle = mdl.rotations[objName].at(i).w * PI / 180.0;
		double anglecos = cos(angle);
		double anglesin = sin(angle);

		if (mdl.rotations[objName].at(i).x == 1)
		{
			rotationMat[1][1] = anglecos;
			rotationMat[2][2] = anglecos;
			rotationMat[2][1] = anglesin;
			rotationMat[1][2] = -anglesin;
		}
		else if (mdl.rotations[objName].at(i).y == 1)
		{
			rotationMat[0][0] = anglecos;
			rotationMat[2][2] = anglecos;
			rotationMat[0][2] = anglesin;
			rotationMat[2][0] = -anglesin;
		}
		else if (mdl.rotations[objName].at(i).z == 1)
		{
			rotationMat[0][0] = anglecos;
			rotationMat[1][1] = anglecos;
			rotationMat[1][0] = anglesin;
			rotationMat[0][1] = -anglesin;
		}

	}
	
	transmat[0][3] = mdl.trans[objName].x;
	transmat[1][3] = mdl.trans[objName].y;
	transmat[2][3] = mdl.trans[objName].z;
	

	return transmat * rotationMat * scalemat;
	 
}

int main(int argc, const char * argv[])
{

	const char * modelFIle;

	if (!glfwInit()) exit(1);

	if (argc == 2) {
		modelFIle = argv[1];
	}
	else if (argc == 1) {
		modelFIle = "../models/scene.model";
	}
	_SimpleModel *mdl = new _SimpleModel();
	ReadSimpleMdl(modelFIle, mdl);
	cout << "----------   CONTROLS   ----------" << endl;
	cout << "Press ',' ('<') to lower light brightness level" << endl;
	cout << "Press '.' ('>') to raise light brightness level" << endl;
	cout << "Press 'r'  to change light mode" << endl;
	cout << "Press 't'  to turn textures on or off" << endl;
	cout << "Press '1'-'2'-'3'-'4' to turn light filters on and off" << endl;
	cout << "Press 'f' to toggle WireFrame Mode" << endl;
	cout << "Press 'w' to move forwards" << endl;
	cout << "Press 's' to move backwards" << endl;
	cout << "Press 'q' to quit" << endl;
	cout << "Left Click and drag mouse to move camera" << endl;
	cout << endl << "----------   Important   ----------" << endl;
	cout <<  "--- PROGRAM STARTS WITH LIGHT LEVELS AT 0, PRESS ',' TO TURN LIGHTS ON! ---" << endl;
	cout << endl << "----------   Notice   ----------" << endl;
	cout << "To use different light filters (1,2,3,4) -" << endl << "You need to turn off the previously used filter before a new one can be enabled." << endl;
	cout << "Fog effects can be seen outside the classroom!" << endl;

	glfwSetErrorCallback(err);

	InitWindow();

	GLenum err = glewInit();
	if (GLEW_OK != err) {
		// Problem: glewInit failed, something is wrong.
		cerr << "Error: " << glewGetErrorString(err) << endl;
		exit(1);
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);
	//glDisable(GL_CULL_FACE);
	program = InitShader("vertex.glsl", "fragment.glsl");
	glUseProgram(program);

	GLuint gModelMatrix = glGetUniformLocation(program, "ModelMatrix");
	GLuint vertLoc = glGetAttribLocation(program, "vPosition");
	GLuint colorLoc = glGetAttribLocation(program, "vColor");
	GLuint normalLoc = glGetAttribLocation(program, "vNormal");
	GLuint emissionLoc = glGetAttribLocation(program, "vEmission");
	GLuint specLoc = glGetAttribLocation(program, "vSpecular");
	GLuint uvLoc = glGetAttribLocation(program, "vUV");

	glUniform3fv(glGetUniformLocation(program, "lightPositions"), mdl->lightPosition.size(), (const GLfloat*)mdl->lightPosition[0]);
	glUniform3fv(glGetUniformLocation(program, "lightColors"), mdl->lightColor.size(), (const GLfloat*)mdl->lightColor[0]);
	GLuint TextureID = glGetUniformLocation(program, "sampler");


	SetupView();

	glClearColor(0.043f, 0.265f, 0.748f, 1.0f);

	GLuint vao[mdl->objects.size()];
	GLuint data[mdl->objects.size()];
	GLuint textures[mdl->objects.size()];
	bool usesTexture[mdl->objects.size()];

	int i = 0;
	int ind[mdl->objects.size()];
	int tmpInd = 0;
	ind[0] = 0;

	map<string, _Object>::iterator obj;

	for (obj = mdl->objects.begin(); obj != mdl->objects.end(); obj++)
	{
		vector<vec3> Vertices;
		vector<vec3> Vindex;
		vector<vec4> Cindex;
		vector<vec3> Nindex;
		vector<vec3> Eindex;
		vector<vec4> Sindex;
		vector<vec2> Tindex;

		ReadObject(obj->second, Vindex, Cindex, Nindex, Eindex, Sindex, Tindex);

		vector<vec4> dataVec;
		usesTexture[i] = checkForTexture(obj->second);
		int G = 5;

		//cout << obj->first << ": " << usesTexture[i] << endl;
		for (size_t j = 0; j < Vindex.size(); j++)
		{
			dataVec.push_back(vec4(Vindex.at(j), 1));
			dataVec.push_back(Cindex.at(j));
			dataVec.push_back(vec4(Nindex.at(j), 1));
			dataVec.push_back(vec4(Eindex.at(j), 1));
			//dataVec.push_back(Sindex.at(j));
			dataVec.push_back(vec4(Tindex.at(j), 1, 1));
		}


		glGenBuffers(1, &data[i]);
		glBindBuffer(GL_ARRAY_BUFFER, data[i]);
		glBufferData(GL_ARRAY_BUFFER, dataVec.size() * sizeof(vec4), dataVec[0], GL_STATIC_DRAW);
		ind[i] = Vindex.size();

		glGenVertexArrays(1, &vao[i]);
		glBindVertexArray(vao[i]);

		glEnableVertexAttribArray(vertLoc);
		glVertexAttribPointer(vertLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, 0);

		glEnableVertexAttribArray(colorLoc);
		glVertexAttribPointer(colorLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, (GLvoid *)(sizeof(vec4)));

		glEnableVertexAttribArray(normalLoc);
		glVertexAttribPointer(normalLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, (GLvoid *)(sizeof(vec4) * 2));

		glEnableVertexAttribArray(emissionLoc);
		glVertexAttribPointer(emissionLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, (GLvoid *)(sizeof(vec4) * 3));

		//glEnableVertexAttribArray(specLoc);
		//glVertexAttribPointer(specLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, (GLvoid *)(sizeof(vec4) * 4));

		if (usesTexture[i])
		{
			glEnableVertexAttribArray(uvLoc);
			glVertexAttribPointer(uvLoc, 4, GL_FLOAT, GL_FALSE, sizeof(vec4) * G, (GLvoid *)(sizeof(vec4) * 4));
			map<string, _Material>::iterator grp;
			for (grp = obj->second.groups.begin(); grp != obj->second.groups.end(); grp++)
			{
				if (grp->second.map_Kd != "")
				{
					string str = grp->second.map_Kd;
					int strlen = 120;
					char chars[strlen];
					strcpy(chars, "../models/textures/");
					strcat(chars, str.c_str());

					int width, height;
					TGAImg ImgData;
					ImgData.Load(chars);
					width = ImgData.GetWidth();
					height = ImgData.GetHeight();
					//unsigned char *ImgData = stbi_load(chars, &width, &height, &nrChannels, 0);
					if (ImgData.GetImg())
					{
						glUniform1i(TextureID, 0);
						glActiveTexture(GL_TEXTURE0);
						glEnable(GL_TEXTURE0);
						glGenTextures(1, &textures[i]);
						glBindTexture(GL_TEXTURE_2D, textures[i]);
						glGenerateMipmap(GL_TEXTURE_2D);
						glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
						glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImgData.GetImg());
					}
					else {
						cout << "object: '" << obj->first << "' Failed to load texture: " << chars << endl;
					}
					//stbi_image_free(ImgData);
					//delete(&ImgData);
				}
			}
		}

		i++;
	}

	glShadeModel(GL_SMOOTH);

	do {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
		int k = 0;
		i = 0;

		for (obj = mdl->objects.begin(); obj != mdl->objects.end(); obj++)
		{
			glUseProgram(program);

			mat4 ModelMatrix = GetModelMatrix(*mdl, obj->first);

			glUniformMatrix4fv(gModelMatrix, 1, GL_TRUE, ModelMatrix);
			glBindVertexArray(vao[i]);
			if (usesTexture[i])
			{
				glEnable(GL_TEXTURE_2D);
				glActiveTexture(GL_TEXTURE0);
			    glBindTexture(GL_TEXTURE_2D, textures[i]);
			}
			else {
				glBindTexture(GL_TEXTURE_2D, 0);
				glDisable(GL_TEXTURE_2D);
			}
	

			map<string, _Material>::iterator grp;
			for (grp = obj->second.groups.begin(); grp != obj->second.groups.end(); grp++)
			{
				if (obj->second.groups[grp->first].Kd.w < 1)
				{
					glEnable(GL_BLEND);
					glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_COLOR, GL_DST_COLOR);
				}
			}
				
			glDrawArrays(GL_TRIANGLES, 0, ind[i]);
			//glDrawElements(GL_TRIANGLES, ind[i], GL_UNSIGNED_INT, NULL);
			i++;
		}

		glfwSwapBuffers(window);
		glfwWaitEvents();
	} while (!glfwWindowShouldClose(window));
		
	for (size_t i = 0; i < mdl->objects.size(); i++)
	{
		glDeleteBuffers(1, &data[i]);
		glDeleteVertexArrays(1, &vao[i]);
		glDeleteTextures(1, &textures[i]);
	}
	glDeleteProgram(program);

	glfwTerminate();

	delete(mdl);
	
}



