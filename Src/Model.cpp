#include "Model.h"
using namespace std;




void ReadSimpleMdl(string filePath, _SimpleModel* mdl) {
	ifstream file(filePath);
	if (file.is_open()) {
		string line;
		string objName;
		bool onLight = false;
		while (getline(file, line))
		{
			if (line.substr(0, 4) == "obj ")
			{ 
				string objPath;
				istringstream s(line.substr(4));
				s >> objName;
				s >> objPath;
				mdl->objects[objName].objFilePath = objPath;
			}
			if (line.substr(0, 6) == "light ")
			{
				onLight = true;
				istringstream s(line.substr(6));
				string lightname;
				s >> lightname;
				vec3 v; s >> v.x; s >> v.y; s >> v.z;
				mdl->lightColor.push_back(v);
			}
			if (line.substr(0, 6) == "trans ")
			{
				if (onLight)
				{
					istringstream s(line.substr(6));
					vec3 v; s >> v.x; s >> v.y; s >> v.z;
					mdl->lightPosition.push_back(v);
					onLight = false;
				}
				else {
					if (objName != "")
					{
						istringstream s(line.substr(6));
						vec3 v; s >> v.x; s >> v.y; s >> v.z;
						mdl->trans[objName] = v;

					}
				}
				
			}
			if (line.substr(0, 6) == "scale ")
			{
				if (objName != "")
				{

					istringstream s(line.substr(6));
					vec3 v; s >> v.x; s >> v.y; s >> v.z;
					mdl->scales[objName] = v;
				}
			}
			if (line.substr(0, 7) == "rotate ")
			{
				if (objName != "")
				{

					istringstream s(line.substr(7));
					vec4 v; s >> v.w; s >> v.x; s >> v.y; s >> v.z;
					mdl->rotations[objName].push_back(v);
				}
			}
			if (line.substr(0, 8) == "include ")
			{
				string modelPath;
				istringstream s(line.substr(8));
				s >> modelPath;
				ReadSimpleMdl("../Models/"+modelPath, mdl);

			}
			if (line[0] == '#')
			{
				/* ignoring this line */
			}
			else
			{
				/* ignoring this line */
			}
		}
	}
}
