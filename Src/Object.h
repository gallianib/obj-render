#pragma once
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <map>
#include <vector>
#include <algorithm>
#include "Angel.h"

struct _Material {
	std::string mtlName;
	float Ns;
	vec3 Ka;
	vec4 Kd;
	vec3 Ks;
	vec3 Ke;
	std::string map_Kd;
	float Ni;
	float d;
	int illum;
};

struct _Object {
	std::string objFilePath;
	std::map<std::string, _Material> groups;
	std::string mtllibName;

};

void ReadMaterial(std::string mtllibname, std::map<std::string, _Material> &mtls);

void ReadObject(_Object &obj,
	std::vector<vec3> &Vindex,
	std::vector<vec4> &Cindex,
	std::vector<vec3> &Nindex,
	std::vector<vec3> &Eindex,
	std::vector<vec4> &Sindex,
	std::vector<vec2> &Tindex);

void Tessellate(vec3 a, vec3 b, vec3 c,
	std::vector<vec3> &tessellated,
	vec4 Kd, std::vector<vec4> &Cindex,
	vec3 Nm, std::vector<vec3> &Nindex,
	vec3 Ke, std::vector<vec3> &Eindex,
	vec4 Ks, std::vector<vec4> &Sindex,
	vec2 TC1, vec2 TC2, vec2 TC3, std::vector<vec2> &Tindex);

vec3 CalcNormal(vec3 p1, vec3 p2, vec3 p3);
vec3 CalcNormal(vec3 v1, vec3 v2);
bool checkForTexture(_Object obj);
bool checkForTexture(std::map<std::string, _Material> mtls);
