 #include "Object.h"
using namespace std;

bool checkForTexture(_Object obj) {
	
	map<string, _Material>::iterator grp;
	for (grp = obj.groups.begin(); grp != obj.groups.end(); grp++)
	{
		if (grp->second.map_Kd != "") {
			return true;
		}
	}
	return false;
}
bool checkForTexture(map<string, _Material> mtls) {

	map<string, _Material>::iterator grp;
	for (grp = mtls.begin(); grp != mtls.end(); grp++)
	{
		if (grp->second.map_Kd != "") {
			return true;
		}
	}
	return false;
}

void Tessellate(vec3 a, vec3 b, vec3 c, vector<vec3> &tessellated, vec4 Kd, vector<vec4> &Cindex, vec3 normal, vector<vec3> &Nindex, vec3 Ke, vector<vec3> &Eindex, vec4 Ks, vector<vec4> &Sindex, vec2 TC1, vec2 TC2, vec2 TC3, vector<vec2> &Tindex){
	/*float ab = 0;
	float ac = 0;
	float bc = 0;

	ab = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2) + pow((b.z - a.z), 2));
	ac = sqrt(pow((c.x - a.x), 2) + pow((c.y - a.y), 2) + pow((c.z - a.z), 2));
	bc = sqrt(pow((c.x - b.x), 2) + pow((c.y - b.y), 2) + pow((c.z - b.z), 2));

	

	//if (ab <= 1 || ac <= 1 || bc <= 1)
	{*/
		tessellated.push_back(a);
		tessellated.push_back(b);
		tessellated.push_back(c);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);
	/*}
	else if ((ab > 1 && ab <= 32) && (ac > 1 && ac <= 32) && (bc > 1 && bc <= 32))
	{
		tessellated.push_back(a);
		tessellated.push_back(b);
		tessellated.push_back(c);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);

	}
	else if ((ab > 1 && ab <= 64) && (ac > 1 && ac <= 64) && (bc > 1 && bc <= 64))
	{
		vec3 abvec = (a + b) / 2;
		vec3 bcvec = (b + c) / 2;
		vec3 acvec = (c + a) / 2;

		tessellated.push_back(a);
		tessellated.push_back(abvec);
		tessellated.push_back(acvec);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);

		tessellated.push_back(b);
		tessellated.push_back(bcvec);
		tessellated.push_back(abvec);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);

		tessellated.push_back(c);
		tessellated.push_back(acvec);
		tessellated.push_back(bcvec);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);

		tessellated.push_back(abvec);
		tessellated.push_back(bcvec);
		tessellated.push_back(acvec);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Cindex.push_back(Kd);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Nindex.push_back(normal);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		Eindex.push_back(Ke);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		//Sindex.push_back(Ks);
		Tindex.push_back(TC1);
		Tindex.push_back(TC2);
		Tindex.push_back(TC3);
	
	}
	else if (ab > 64 || ac > 64 || bc > 64)
	{
		vec3 abvec = (a + b) / 2;
		vec3 acvec = (c + a) / 2;
		vec3 bcvec = (b + c) / 2;

		Tessellate(a, abvec, acvec, tessellated,Kd,Cindex, normal, Nindex, Ke, Eindex, Ks, Sindex,  TC1,  TC2,  TC3, Tindex);
		Tessellate(b, bcvec, abvec, tessellated, Kd, Cindex, normal, Nindex, Ke, Eindex, Ks, Sindex,  TC1,  TC2,  TC3, Tindex);
		Tessellate(c, acvec, bcvec, tessellated, Kd, Cindex, normal, Nindex,  Ke, Eindex, Ks, Sindex,  TC1,  TC2,  TC3, Tindex);
		Tessellate(abvec, bcvec, acvec, tessellated, Kd, Cindex, normal, Nindex, Ke, Eindex, Ks, Sindex, TC1, TC2, TC3, Tindex);
	}*/
}
void ReadMaterial(std::string mtllibname, std::map<std::string, _Material> &mtls) {
	ifstream in("../Models/Objects/" + mtllibname, ios::in);
	if (!in)
	{
		cerr << "Cannot open " << mtllibname << endl; exit(1);
	}
	string line;
	_Material newmtl;
	int read = 0;
	while (getline(in, line))
	{
		if (line.substr(0, 7) == "newmtl ")
		{
			if (read == 1)
			{
				mtls[newmtl.mtlName] = newmtl;

				newmtl.mtlName = "";
				newmtl.map_Kd = "";
				newmtl.Kd = vec4(0);
				newmtl.Ke = vec3(0);
				newmtl.Ks = vec3(0);
				newmtl.Ns = 0.0f;
				read = 0;
			}
			string sss;
			istringstream s(line.substr(7));
			s >> sss;
			newmtl.mtlName = sss;
			read = 1;
		}
		if (line.substr(0, 7) == "map_Kd ")
		{
			string map_kd;
			istringstream s(line.substr(7));
			s >> map_kd;
			newmtl.map_Kd = map_kd;
		}
		if (line.substr(0, 3) == "Kd ")
		{
			istringstream s(line.substr(3));
			
			vec4 kd; s >> kd.x; s >> kd.y; s >> kd.z; s >> kd.w;
			if (kd.w == 0) {
				kd.w = 1;
			}
			newmtl.Kd = kd;
		}
		if (line.substr(0, 3) == "Ke ")
		{
			istringstream s(line.substr(3));
			vec3 ke = vec3(1); s >> ke.x; s >> ke.y; s >> ke.z;
			newmtl.Ke = ke;
		}
		if (line.substr(0, 3) == "Ks ")
		{
			istringstream s(line.substr(3));

			vec3 ks = vec3(1); s >> ks.x; s >> ks.y; s >> ks.z;
			newmtl.Ks = ks;
		}
		if (line.substr(0, 3) == "Ns ")
		{
			istringstream s(line.substr(3));

			float Ns;
			s >> Ns;
			newmtl.Ns = Ns;
		}
	}
	mtls[newmtl.mtlName] = newmtl;
}
void ReadObject(_Object &obj, std::vector<vec3> &Vindex, std::vector<vec4> &Cindex, std::vector<vec3> &Nindex, std::vector<vec3> &Eindex, std::vector<vec4> &Sindex, std::vector<vec2> &Tindex) {
	ifstream in("../Models/"+obj.objFilePath, ios::in);
	if (!in)
	{
		cerr << "Cannot open " << obj.objFilePath << endl; exit(1);
	}
	string currentObj;
	string currentMtl;
	string line;
	vector<vec3> tnormal;
	vector<vec3> Vertices;
	vector<vec2> tmpTC;
	map<string, _Material> mtls;
	while (getline(in, line))
	{
		if (line.substr(0, 2) == "g ")
		{
			istringstream s(line.substr(2));
			s >> currentObj;
			obj.groups[currentObj];
		}
		if (line.substr(0, 2) == "o ")
		{
			istringstream s(line.substr(2));
			s >> currentObj;
			obj.groups[currentObj];
		}
		if (line.substr(0, 2) == "v ")
		{
			istringstream s(line.substr(2));
			vec3 v; s >> v.x; s >> v.y; s >> v.z;
			Vertices.push_back(v);
		}
		if (line.substr(0, 3) == "vn ")
		{
			istringstream s(line.substr(3));
			vec3 vn; s >> vn.x; s >> vn.y; s >> vn.z;
			tnormal.push_back(vn);
		}
		if (line.substr(0, 3) == "vt ")
		{
			istringstream s(line.substr(3));
			vec2 vt; s >> vt.x; s >> vt.y;
			tmpTC.push_back(vt);
		}
		if (line.substr(0, 7) == "usemtl ")
		{
			istringstream s(line.substr(7));
			s >> currentMtl;
			obj.groups[currentObj] = mtls[currentMtl];
		}
		if (line.substr(0, 7) == "mtllib ")
		{
			istringstream s(line.substr(7));
			s >> obj.mtllibName;
			ReadMaterial(obj.mtllibName, mtls);
		}
		else if (line.substr(0, 2) == "f ")
		{
			string delimiter = "/";
			int vertexIndex[4] = { 0,0,0,0 };
			int textureIndex[4] = { 0,0,0,0 };
			int normalIndex[4] = { 0,0,0,0 };
			int wordCount = 0;
			istringstream s(line.substr(2));
			for (string word; s >> word; ) {
				wordCount++;
				size_t pos = 0;
				string token;
				int count = 0;
				while ((pos = word.find(delimiter)) != std::string::npos) {
					token = word.substr(0, pos);
					count++;
					if (token != "") {
						int t = stoi(token);
						if (count == 1) {
							vertexIndex[wordCount - 1] = t;
						}
						if (count == 2) {
							textureIndex[wordCount - 1] = t;
						}
					}
					word.erase(0, pos + delimiter.length());
				}
				if (count == 2 && word != "")
				{
					normalIndex[wordCount - 1] = stoi(word);
				}
				if ((pos = word.find(delimiter)) == std::string::npos && count == 0)
				{
					vertexIndex[wordCount - 1] = stoi(word);
				}
			}
			vec4 Kd = vec4(1);
			Kd = obj.groups[currentObj].Kd;
			vec3 Ke = vec3(1);
			Ke = obj.groups[currentObj].Ke;
			vec3 Ks = vec3(1);
			Ks = obj.groups[currentObj].Ks;
			float Ns = obj.groups[currentObj].Ns;
			vec4 nKs = vec4(Ks,Ns);
			vec2 TC1 = vec2(2);
			vec2 TC2 = vec2(2);
			vec2 TC3 = vec2(2);
			bool usesTexture = false;
			if (obj.groups[currentObj].map_Kd != "") {
				usesTexture = true;
			}
			
			try {
				//cout << currentObj << ": " << obj.groups[currentObj].mtlName << ".Kd = " << Kd << endl;
				if (wordCount == 3)
				{
					if (usesTexture)
					{
						 TC1 = tmpTC.at(textureIndex[0] - 1);
						 TC2 = tmpTC.at(textureIndex[1] - 1);
						 TC3 = tmpTC.at(textureIndex[2] - 1);
					}

					vec3 normal = CalcNormal(Vertices.at(vertexIndex[0] - 1), Vertices.at(vertexIndex[1] - 1), Vertices.at(vertexIndex[2] - 1));

					Tessellate(Vertices.at(vertexIndex[0] - 1), Vertices.at(vertexIndex[1] - 1), Vertices.at(vertexIndex[2] - 1), Vindex, Kd, Cindex, normal, Nindex, Ke, Eindex, nKs, Sindex, TC1, TC2, TC3, Tindex);
				}
				else if (wordCount == 4)
				{
					if (usesTexture)
					{
						TC1 = tmpTC.at(textureIndex[0] - 1);
						TC2 = tmpTC.at(textureIndex[1] - 1);
						TC3 = tmpTC.at(textureIndex[2] - 1);
					}
					vec3 normal1 = CalcNormal(Vertices.at(vertexIndex[0] - 1), Vertices.at(vertexIndex[1] - 1), Vertices.at(vertexIndex[2] - 1));
					vec3 normal2 = CalcNormal(Vertices.at(vertexIndex[2] - 1), Vertices.at(vertexIndex[3] - 1), Vertices.at(vertexIndex[0] - 1));

					Tessellate(Vertices.at(vertexIndex[0] - 1), Vertices.at(vertexIndex[1] - 1), Vertices.at(vertexIndex[2] - 1), Vindex, Kd, Cindex, normal1, Nindex, Ke, Eindex, nKs, Sindex, TC1, TC2, TC3, Tindex);
					if (usesTexture)
					{
						TC1 = tmpTC.at(textureIndex[2] - 1);
						TC2 = tmpTC.at(textureIndex[3] - 1);
						TC3 = tmpTC.at(textureIndex[0] - 1);
					}
					Tessellate(Vertices.at(vertexIndex[2] - 1), Vertices.at(vertexIndex[3] - 1), Vertices.at(vertexIndex[0] - 1), Vindex, Kd, Cindex, normal2, Nindex, Ke, Eindex, nKs, Sindex, TC1, TC2, TC3, Tindex);
				}
			}
			catch (exception e) {}
		}
		else if (line[0] == '#'){}
		else{}
	}
}
vec3 CalcNormal(vec3 p1, vec3 p2, vec3 p3) {
	vec3 U = p1 - p2;
	vec3 V = p1 - p3;

	vec3 normal = normalize(cross(U,V));

	return normal;
}
vec3 CalcNormal(vec3 v1, vec3 v2) {

	vec3 normal = normalize(cross(v1, v2));

	return normal;
} 