#version 140

in vec4 vPosition;
in vec4 vColor;
in vec4 vEmission;
//in vec4 vSpecular;
in vec4 vNormal;
in vec4 vUV;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat4 ModelMatrix;


out vec4 color;
out vec4 emission;
out vec3 FragPos;
out vec3 normal;
out vec3 position;
out vec3 UV;
out vec3 cameraPos;
//out vec4 specular;

void main() 
{
	mat4 ModelViewProjection = Projection * ModelView * ModelMatrix;
    gl_Position = ModelViewProjection * vPosition;
	UV = vec3(vUV);
	emission = vEmission;
	FragPos = vec3(ModelMatrix * vPosition);
	color = vColor;
	position = vec3(vPosition);
	mat4 inverseView = inverse(ModelView);
	cameraPos = vec3(inverseView[3]);
	//specular = vSpecular;
	normal = normalize(vec3(vNormal));

} 

