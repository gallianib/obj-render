#version 140


int numLights = 16;

uniform sampler2D sampler;
uniform vec3 lightPositions[16];
uniform vec3 lightColors[16];

uniform float settings1;
uniform float settings2;
uniform float settings3;
uniform float settings4;
uniform float lightMode;
uniform float lightlevel;
uniform float textureSettings;

vec3 TESTLP = vec3(163, 86.8, -153);
vec3 TESTLC = vec3(.9, .9, .9);

in vec4 color;
in vec3 normal;
in vec3 UV;
in vec4 emission;
in vec3 FragPos;
in vec3 position;
in vec3 cameraPos;
//in vec4 specular;

float cutoff = .95;

vec4 CalcPointLight(vec3 lightPosition, vec3 lightColor);
vec4 CalcSpotLight(vec3 lightPosition, vec3 lightColor, vec3 coneDirection);
vec4 ComputeDirectional(vec3 lightColor);
vec4 ComputeDiffuse();
vec4 calcSpec(vec3 lightPosition, vec3 lightColor);
vec4 ComputeSpecular();

vec4 ComputeDiffuse(){
	vec4 sumDiffuse = vec4(0,0,0,0);

	for	(int i = 0; i < numLights; i++){
		vec3 lightPosition = lightPositions[i];
		vec3 lightColor = lightColors[i];

		if(lightMode == 0){
			sumDiffuse += CalcPointLight(lightPosition,lightColor);
		}else{
			sumDiffuse += CalcSpotLight(lightPosition,lightColor,vec3(0,-1,0));
		}
	}
	return clamp(sumDiffuse, 0, 1);
}

vec4 ComputeSpecular(){
	vec4 sumSpecular = vec4(0,0,0,0);

	for	(int i = 0; i < numLights; i++){
		vec3 lightPosition = lightPositions[i];
		vec3 lightColor = lightColors[i];
		sumSpecular += calcSpec(lightPosition,lightColor);	
	}
	return clamp(sumSpecular, 0, 1);
}

vec4 CalcSpotLight(vec3 lightPosition, vec3 lightColor, vec3 coneDirection)
{
	vec3 LightDir = normalize(lightPosition - FragPos);
	float distance = length(lightPosition-FragPos);
	float cosTheta = max(0, dot(coneDirection,-LightDir));

	if (cosTheta < cutoff){
		return vec4(0);
	}else {
		return (vec4(lightColor,1) * cosTheta) / ((distance*distance));
	}
} 

vec4 ComputeDirectional(vec3 lightColor)
{
	return (vec4(lightColor,1));
}

vec4 CalcPointLight(vec3 lightPosition, vec3 lightColor)
{
	vec3 LightDir = normalize(lightPosition - FragPos);
	float distance = length(lightPosition-FragPos);
	float cosTheta = max(0, dot(normalize(normal),LightDir));
	return (vec4(lightColor,1) * cosTheta) / ((distance*distance));
} 

vec4 calcSpec(vec3 lightPosition, vec3 lightColor){

	vec3 incidenceVector = normalize(lightPosition - FragPos);
	vec3 reflectionVector = reflect(-incidenceVector, normal);
	vec3 viewPos = normalize(cameraPos - FragPos); 
	float specularCoefficient = pow(max(dot(viewPos, reflectionVector), 0.0), 256);

	vec3 specularComp = (0.2 * specularCoefficient * lightColor.xyz);
	vec4 specularComponent = vec4(specularComp,1);
	return specularComponent;
}

void main() 
{ 
 vec4 ambient =  vec4(0.1, 0.1, 0.1, 1);
 vec4 diffuse = lightlevel * ComputeDiffuse();
 vec4 spec = 1 * ComputeSpecular();
 vec4 directional;

  float dist = abs(cameraPos.z - FragPos.z);
 float maxf = 400;
 float minf = 125;

 float fog = clamp((maxf - dist) / (maxf - minf), 0.0, 1.0);
 vec4 fogColor = vec4(0.2,0.2,0.2,1);

 if(FragPos.x < -125){
	directional = 1 * ComputeDirectional(TESTLC);  

}else{
	directional = 0.2 * ComputeDirectional(TESTLC);  
	fog = 1;
}

 vec4 rgba = (emission + clamp(ambient, 0, 1) + clamp(spec, 0, 1) + clamp(diffuse, 0, 1) + clamp(directional, 0, 1));
 vec3 gamma = vec3(1.0/2.2);
 if(UV.z == 1 && UV.x != 2){
	 if(textureSettings > 0){
		 rgba = rgba * texture(sampler, vec2(UV));
	}else if(settings1 > 0){
		rgba = (1-(rgba * color)); 	
	}else if (settings2 > 0){
		rgba = rgba * vec4(1,0,0,1);
	}else if (settings3 > 0){
		rgba = rgba * vec4(0,1,0,1);
	}else if (settings4 > 0){
		rgba = rgba * vec4(0,0,1,1);
	}else {
		rgba = rgba * color;
	}
}else{
	if(settings1 > 0){
		rgba = (1-(rgba * color));
	}else if (settings2 > 0){
		rgba = rgba * vec4(1,0,0,1);
	}else if (settings3 > 0){
		rgba = rgba * vec4(0,1,0,1);
	}else if (settings4 > 0){
		rgba = rgba * vec4(0,0,1,1);
	}else {
		rgba = rgba * color;
	}
}

 gl_FragColor = mix(fogColor,clamp(vec4(pow(rgba.xyz,gamma), color.a),0,1),fog);
} 

